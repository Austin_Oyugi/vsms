<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class vehicle_request extends CI_Controller
{
    public function __construct()
    {

        parent::__construct();
        $this->load->model('model_vehicle');
        $this->load->model('model_manufacturer');
        $this->load->model('model_car_model');
    }

    public function add()
    {
        if($this->input->post('buttonSubmit')) {
            $data['message'] = '';

            $this->form_validation->set_rules('manufacturer_id', 'Manufacturer', 'required');
            $this->form_validation->set_rules('model_id', 'Model', 'required');
            $this->form_validation->set_rules('category', 'Category ', 'required');
            $this->form_validation->set_rules('b_price', 'Buying Price ', 'required');
            $this->form_validation->set_rules('mileage', 'Mileage', 'required');
            $this->form_validation->set_rules('add_date', 'Adding Date', 'required');
            $this->form_validation->set_rules('registration_year', 'Registration Year Date', 'required');
            $this->form_validation->set_rules('insurance_id', 'Insurance ID', 'required');
            $this->form_validation->set_rules('gear', 'Gear', 'required');
            $this->form_validation->set_rules('doors', 'Number of Doors', 'required');
            $this->form_validation->set_rules('seats', 'Number of Seats', 'required');
            $this->form_validation->set_rules('tank', 'Tank capacity', 'required');
            $this->form_validation->set_rules('e_no', 'Engine No', 'required');
            $this->form_validation->set_rules('c_no', 'Chasis No', 'required');
            $this->form_validation->set_rules('v_color', 'Color', 'required');
            $this->form_validation->set_rules('phone_num','Phone Number', 'required' );

            if($this->form_validation->run() == FALSE)
            {
                //$data['vRow'] = $this->model_vehicle->get($cid);
                $this->load->view('indexv/post_car');
            }
            else{
                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_width']    = '2048';
                $config['max_height']   = '2048';
                $this->load->library('upload', $config);


                $manufacturer_name = $this->input->post('manufacturer_id');
                $model_name = $this->input->post('model_id');
                $category = $this->input->post('category');
                $b_price = $this->input->post('b_price');

                $mileage = $this->input->post('mileage');
                $add_date = $this->input->post('add_date');
                $status = "available";
                $registration_year = $this->input->post('registration_year');
                $insurance_id = $this->input->post('insurance_id');
                $gear = $this->input->post('gear');
                $doors = $this->input->post('doors');
                $seats = $this->input->post('seats');
                $tank = $this->input->post('tank');
                $e_no = $this->input->post('e_no');
                $c_no = $this->input->post('c_no');
                $u_id = $this->session->userdata('id');
                $v_color = $this->input->post('v_color');
                $featured = $this->input->post('featured');
                $phone_num = $this->input->post('phone_num');

                $this->upload->do_upload('image');
                $data = $this->upload->data('image');
                $image= $data['file_name'];

                $this->model_vehicle->insert($featured,$image,$manufacturer_name,$model_name,$category,$b_price,$mileage,$add_date,$status,$registration_year,$insurance_id,$gear,$doors,$seats,$tank,$e_no,$c_no,$u_id,$v_color, $phone_num);
                $this->session->set_flashdata('message','Vehicle Successfully Created.');
                redirect(base_url('index_search/add_vehicle'));
                echo "<script>alert('success')</script>";

            }
        }
        else{
            redirect(base_url('index_search/add_vehicle'));
        }
    }
}