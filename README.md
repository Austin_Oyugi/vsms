

#Features : 
Front End with Vehicles Added from Back-end with details
Back-end login system for admins, employees
Different Controlling for Admin and Employee
New Employee Adding for doing Sales , Adding new vehicles

	Admin Features : # Add, Edit , Delete almost anything(Including new users), Sell vehicles, See customer Details
	Employee Features : # Add vehicles only and Sell them to new clients with their details
	-------------------------------------------------------------------------
	Others : Used CodeIgniter Active record for DB query, Basic Parser use of codeigniter, 
	uses cool template for backend with a lot 	 of effects, form validation with codeigniter Database is included in the folder.
	Uses Datatables with sorting , filtering , and date to date range for getting result
	Do change the database config to run on your server / localhost

Admin User : admin@admin.com
Password : admin

Employee User : employee@employee.com
Password : employee
