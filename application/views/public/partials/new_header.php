<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Online Car Sale System </title>
    <meta name="description" content="Scarica gratis il nostro Template HTML/CSS GARAGE. Se avete bisogno di un design per il vostro sito web GARAGE può fare per voi. Web Domus Italia">
    <meta name="author" content="Web Domus Italia">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/indextp/source/bootstrap-3.3.6-dist/css/bootstrap.css';?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/indextp/source/font-awesome-4.5.0/css/font-awesome.css';?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/indextp/style/slider.css';?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/indextp/style/mystyle.css';?>">
</head>
<body>
<!-- Header -->
<div class="allcontain">
    <div class="header">
        <ul class="socialicon">
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
        </ul>
        <ul class="givusacall">
            <li>Give us a call : +254721373685</li>
        </ul>
        <ul class="logreg">
            <li><a href="<?php echo base_url('login'); ?>">Login</a></li>
            <li><a href="#"><span class="register">Register</span></a></li>
        </ul>
    </div>
    <!-- Navbar Up -->
    <nav class="topnavbar navbar-default topnav">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed toggle-costume" data-toggle="collapse" data-target="#upmenu" aria-expanded="false">
                    <span class="sr-only"> Toggle navigaion</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand logo" href="#"><img src="<?php echo base_url().'assets/indextp/image/logo1.png';?>" alt="logo"></a>
            </div>
        </div>
        <div class="collapse navbar-collapse" id="upmenu">
            <ul class="nav navbar-nav" id="navbarontop">
                <li class="active"><a href="<? echo base_url(); ?>">HOME</a> </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle"	data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">CATEGORIES <span class="caret"></span></a>
                    <ul class="dropdown-menu dropdowncostume">
                        <li><a href="#">Sport</a></li>
                        <li><a href="#">Old</a></li>
                        <li><a href="#">New</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">DEALERS <span class="caret"></span></a>
                    <ul class="dropdown-menu dropdowncostume">
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="3">3</a></li>
                    </ul>
                </li>
                <li>
                    <a href="contact.html">CONTACT</a>

                </li>
                <button><span class="postnewcar"><a href="<? echo base_url("index_search/add_vehicle"); ?>" class="postnewcar">POST NEW CAR</a></span></button>
<!--                <button><span class="postnewcar">POST NEW CAR</span></button>-->
            </ul>
        </div>
    </nav>
</div>

