<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class index_search extends CI_Controller
{
    public function index()
    {
        $this->load->view("indexv/index");
    }

    public function add_vehicle()
    {
        $this->load->view("indexv/post_car");
    }
}