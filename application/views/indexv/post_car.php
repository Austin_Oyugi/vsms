<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Online Car Sale System </title>
    <meta name="description" content="Scarica gratis il nostro Template HTML/CSS GARAGE. Se avete bisogno di un design per il vostro sito web GARAGE può fare per voi. Web Domus Italia">
    <meta name="author" content="Web Domus Italia">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/indextp/source/bootstrap-3.3.6-dist/css/bootstrap.css';?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/indextp/source/font-awesome-4.5.0/css/font-awesome.css';?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/indextp/style/slider.css';?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/indextp/style/mystyle.css';?>">
</head>
<body>
<!-- Header -->
<div class="allcontain">
    <div class="header">
        <ul class="socialicon">
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
        </ul>
        <ul class="givusacall">
            <li>Give us a call : +254721373685</li>
        </ul>
        <ul class="logreg">
            <li><a href="<?php echo base_url('login'); ?>">Login</a></li>
            <li><a href="#"><span class="register">Register</span></a></li>
        </ul>
    </div>
    <!-- Navbar Up -->
    <nav class="topnavbar navbar-default topnav">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed toggle-costume" data-toggle="collapse" data-target="#upmenu" aria-expanded="false">
                    <span class="sr-only"> Toggle navigaion</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                </div>
        </div>
        <div class="collapse navbar-collapse" id="upmenu">
            <ul class="nav navbar-nav" id="navbarontop">
                <li class="active"><a href="<? echo base_url(); ?>">HOME</a> </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle"	data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">CATEGORIES <span class="caret"></span></a>
                    <ul class="dropdown-menu dropdowncostume">
                        <li><a href="#">Sport</a></li>
                        <li><a href="#">Old</a></li>
                        <li><a href="#">New</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">DEALERS <span class="caret"></span></a>
                    <ul class="dropdown-menu dropdowncostume">
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="3">3</a></li>
                    </ul>
                </li>
                <li>
                    <a href="contact.html">CONTACT</a>

                </li>
            </ul>
        </div>
    </nav>
</div>

<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div>
            <?php echo validation_errors(); ?>
            <?php echo form_open_multipart('vehicle_request/add'); ?>
            <fieldset>
                <div class="row">
                    <div class="col-xs-6">
                        <label>Vehicle Manufacturer</label>
                        <select class="form-control" name="manufacturer_id" id="parent_cat">
                            {manufacturers}
                            <option value="{id}">{manufacturer_name}</option>
                            {/manufacturers}
                        </select>
                    </div>
                    <div class="col-xs-6">
                        <label>Vehicle Model</label>
                        <select class="form-control" name="model_id" >
                            {models}
                            <option value="{id}">{model_name}</option>
                            {/models}
                        </select>
                    </div>
                </div>

                <br>

                <div class="row">
                    <div class="col-xs-6">
                        <label>Vehicle Category</label>
                        <select class="form-control" name="category" >
                            <option value="Subcompact">Subcompact</option>
                            <option value="Compact">Compact</option>
                            <option value="Mid-size">Mid-size</option>
                            <option value="Full-size">Full-size</option>
                            <option value="Mini-Van">Mini-Van</option>
                        </select>
                    </div>
                    <div class="col-xs-6">
                        <br>
                        <input type="number" step="any" class="form-control" name="b_price" placeholder="Buying Price" required>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-xs-6">
                        <br>
                        <label for="gear">Gear Type:</label>
                        <select name="gear" id="gear" class="form-control">
                            <option value="auto">Auto</option>
                            <option value="manual">Manual</option>
                        </select>
                    </div>
                    <div class="col-xs-6">
                        <br>
                        <label for="mileage">Mileage:</label>
                        <input type="text" step="any" class="form-control" name="mileage" placeholder="Mileage(km)" required>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-xs-6">
                        <br>
                        <input class="form-control" name="e_no" placeholder="Engine Number" required>
                    </div>
                    <div class="col-xs-6">
                        <br>
                        <input class="form-control" name="c_no" placeholder="Chassis Number" required>
                    </div>
                </div>

                <br>

                <div class="row">
                    <div class="col-xs-6">
                        <label>Add Date</label>
                        <input type="Date"class="form-control" name="add_date"  value="<?php echo date("Y-m-d"); ?>" >
                    </div>
                    <div class="col-xs-6">
                        <br>
                        <input type="number" class="form-control" name="doors" placeholder="No of Doors" required>
                    </div>
                </div>

                <br>

                <div class="row">
                    <div class="col-xs-6">
                        <br>
                        <input type="number"class="form-control" name="registration_year" placeholder="Registration Year (YYYY)" required>
                    </div>
                    <div class="col-xs-6">
                        <br>
                        <input type="number" class="form-control" name="insurance_id" placeholder="Insurance ID" required>
                    </div>
                </div>

                <br>

                <div class="row">
                    <div class="col-xs-6">
                        <input type="number"class="form-control" name="seats" placeholder="No of seats" required>
                    </div>
                    <div class="col-xs-6">
                        <input type="number" step="any" class="form-control" name="tank" placeholder="Tank Capacity(litters)" required>
                    </div>
                </div>
                <br>

                <div class="row">
                    <div class="col-xs-6">
                        <input type="text"class="form-control" name="v_color" placeholder="Color" required>
                    </div>
                    <div class="col-xs-6">
                        <input type="file" class="form-control" name="image" >
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-xs-6">
                        <label for="gear">Featured ?</label>
                        <select name="featured" id="featured" class="form-control">
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                        </select>

                        <label for="phone_num"> Phone Number</label>
                        <input id="phone_num" type="text" class="form-control" name="phone_num" placeholder="Phone Number">
                    </div>
                </div>
                <br>
                <input class="btn btn-primary" type="submit" name="buttonSubmit" value="Add New Vehicle" />

            </fieldset>
            </form>
            <br>
        </div>
    </div> <!-- /row -->
</div>
</body>
</html>

